package hello.rest;

import hello.jms.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class PublisherController {
    private Publisher publisher;

    @Autowired
    public PublisherController(Publisher publisher) {
        this.publisher = publisher;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void index(@RequestBody String message) {
        publisher.publish(message);
    }
}
