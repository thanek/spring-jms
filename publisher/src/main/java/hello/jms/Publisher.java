package hello.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

@Component
public class Publisher {
    private Logger logger = LoggerFactory.getLogger(Publisher.class);
    private JmsTemplate jmsTemplate;

    @Autowired
    public Publisher(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    public void publish(String message) {
        logger.info("Publishing {}", message);
        MessageCreator messageCreator = session -> session.createTextMessage(message);
        jmsTemplate.send("public", messageCreator);
    }
}
