package hello.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer {
    private Logger logger = LoggerFactory.getLogger(Consumer.class);

    @JmsListener(destination = "${consumer.queue}", concurrency = "${consumer.concurrency}")
    public void consume(String message) {
        logger.info("Received: [{}]", message);
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("Consumed: [{}]", message);
    }
}
